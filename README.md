# Image cropping

**step 1** 

```pip install -r requirements.txt```

**step 2**

create DataBase in MySQL

**step 3**

create `.env` file and add configuration for **DB**
sample: 

DB_CONFIG=mysql://username:password@host/DB_name

```
touch .env && echo "DB_CONFIG=mysql://ALEX:password123@localhost/flask_db" >> .env
```

**step 4**

create tables

 just import the **db** object from an interactive Python shell(`$ python`) and run the SQLAlchemy.create_all() method to create the tables and database:


```
>>> from app import db
>>> db.create_all()
>>> exit()
```

**step 5**

create server:
```
python app.py
```

links in app: 

/ -index

/login - for login

/sign_in - for registration

/profile - user profile page

/sign_out - link for log out

/admin - admin panel  (admin is first registered user )
