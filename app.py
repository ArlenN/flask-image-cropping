import os


from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from dotenv import load_dotenv


UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/userFiles'

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


dotenv_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.env')
load_dotenv(dotenv_path)

app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DB_CONFIG')

db = SQLAlchemy(app)
login = LoginManager(app)


from views import *


if __name__ == '__main__':
    app.run()
