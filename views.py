import gc


from flask import flash, render_template, send_from_directory, abort, json, request, redirect, \
    url_for
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.utils import secure_filename
from app import app, db, login, os
from utils import crop, allowed_file
from model import User, File
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_caching import Cache


cache = Cache(app, config={'CACHE_TYPE': 'simple'})


class MyModelViewUser(ModelView):
    column_list = ('first_name', 'last_name', 'email')
    form_columns = ('first_name', 'last_name', 'email', 'password')

    def is_accessible(self):
        return current_user.is_authenticated and current_user.id == 1

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login'))


class MyModelViewFile(ModelView):
    can_create = False
    can_edit = False

    def is_accessible(self):
        return current_user.is_authenticated and current_user.id == 1

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login'))


class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.id == 1

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('login'))


admin = Admin(app, index_view=MyAdminIndexView())
admin.add_view(MyModelViewUser(User, db.session))
admin.add_view(MyModelViewFile(File, db.session))


@login.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@login.unauthorized_handler
def unauthorized():
    # do stuff
    return redirect(url_for('login'))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        user = User.query.filter_by(email=email).first()
        if user != None and user.password == password:
            login_user(user)
            return redirect(url_for('profile'))
        flash("wrong password")
        return redirect(request.url)
    return render_template('login.html')


@app.route('/sign_in', methods=['GET', 'POST'])
def sign_in():
    if request.method == 'POST':
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        email = request.form['email']
        password = request.form['password']
        password2 = request.form['password2']
        if password != password2:
            flash("Passwords do not match")
            return redirect(request.url)
        if first_name == '' or last_name == '' or email == '' or password == '' or password2 == '':
            flash("Fill in all the fields")
            return redirect(request.url)
        user = User({'first_name': first_name, 'last_name': last_name,
                     'email': email, 'password': password})
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('login'))
        # login(user)
    return render_template('registration.html')


@app.route('/sign_out')
@login_required
def sign_out():
    logout_user()
    return redirect(url_for('index'))


@app.route('/new', methods=['GET', 'POST'])
@login_required
def new():
    gc.collect()
    cache.clear()
    if request.method == 'POST':
        x = int(request.form['x'])
        y = int(request.form['y'])
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            directory = app.config['UPLOAD_FOLDER'] + '/' + str(current_user.id)
            if not os.path.exists(directory):
                os.makedirs(directory)
            file.save(os.path.join(directory, filename))
            crop(os.path.join(directory, filename), (x, y))
            os.remove(os.path.join(directory, filename))
            return redirect(url_for('profile'))
    return render_template('new.html')


@app.route('/profile')
@login_required
def profile():
    files = File.query.filter_by(user_id=current_user.id).all()
    files_dict = {}
    if len(files) != 0:
        for file in files:
            if file.name[2:-4] in files_dict:
                files_dict[file.name[2:-4]].append(file)
            else:
                files_dict[file.name[2:-4]] = [file]
    return render_template('profile.html', Files=files_dict, user_id=current_user.id, )


@app.route('/delete/file', methods=['DELETE'])
@login_required
def delete_file():
    file = File.query.filter_by(name=request.form['name']).first()
    db.session.delete(file)
    db.session.commit()
    os.remove(app.config['UPLOAD_FOLDER'] + '/' + str(current_user.id) + '/' + request.form['name'])
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/userFiles/<path:path>')
def get_resource(path):
    if current_user.is_anonymous or str(current_user.id) != path.split('/', 1)[0]:
        abort(404)
    return send_from_directory('userFiles', path)


