import zipfile
import time

from PIL import Image
from flask_login import current_user
from app import app, db
from model import User, File


ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def crop(image_path, cropped_quantity):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    saved_location = app.config['UPLOAD_FOLDER']
    image_obj = Image.open(image_path)
    width = int(image_obj.size[0] / cropped_quantity[0])
    height = int(image_obj.size[1] / cropped_quantity[1])
    cur_user = User.query.get(current_user.id)
    x, y, x1, y1 = 0, 0, width, height
    files_names = []
    time_for_name = str(int(time.time()))
    zipf = zipfile.ZipFile('userFiles/'+str(current_user.id)+'/images'+time_for_name+'.zip', 'w', zipfile.ZIP_DEFLATED)
    for i in range(0, cropped_quantity[1]):
        for j in range(0, cropped_quantity[0]):
            cropped_image = image_obj.crop((x, y, x1, y1))
            file_name = str(i) + str(j) + time_for_name + '.' + image_obj.filename.rsplit('.', 1)[1].lower()
            file_db = File(name=file_name, pair=str(i)+str(j), user=cur_user)
            db.session.add(file_db)
            db.session.commit()
            cropped_image.save(
                saved_location + '/' + str(current_user.id)+'/' + file_name)
            zipf.write(
                'userFiles/' + str(current_user.id) + '/' + file_name)
            files_names.append(str(i) + str(j) + '.' + image_obj.filename.rsplit('.', 1)[1].lower())
            x = x + width
            x1 = x1 + width
        x, x1 = 0, width
        y = y + height
        y1 = y1 + height
